FROM prom/blackbox-exporter:master

LABEL maintainer='Drew Stinnett <drew.stinnett@duke.edu>' \
      vendor='Duke University, Office of Information Technology, Automation' \
      architecture='x86_64' \
      summary='Prometheus Blackbox Service' \
      description='Duke implementation of the Prometheus Blackbox Service' \
      distribution-scope='private'

ADD blackbox.yml /etc/blackbox_exporter/blackbox.yml

ENTRYPOINT /bin/blackbox_exporter --config.file=/etc/blackbox_exporter/blackbox.yml --history.limit=5
