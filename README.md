# Prometheus Blackbox Exporter Deployment

This will deploy a 'Duke-ified' version of the
[blackbox_exporter](https://github.com/prometheus/blackbox_exporter) image from
Prometheus

See the [Google SRE
Book](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/)
for background

## OKD/OpenShift Setup

Create a new project (namespace) withing OKD

```
$ oc new-project my-blackbox-project
```

Create the application with:

```
$ oc create -f okd/app.yaml -n my-blackbox-project
```

Note that by default, the exporter is only available to 10.0.0.0/8.  You can
change this by editing the `haproxy.router.openshift.io/ip_whitelist`
annotation within the route
